const request = require('supertest');
const app = require('../app');

describe('Home Page Validation', function() {
    it('Displays Title', function(done) {
	request(app)
	    .get('/')
	    .expect(/FF4TV/)
	    .expect(/Welcome to FF4TV/, done)
    });
});

/*
describe('Endpoints Reachable', function() {
    it('Home page', function(done) {
	request(app)
	    .get('/')
	    .expect(200, done)
    });
    it('Edit Page', function(done) {
	request(app)
	    .get('/edit')
	    .expect(200, done)
    });
});
*/

