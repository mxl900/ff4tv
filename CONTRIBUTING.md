# Cloning

Clone the repository when you need to grab fresh source code.

```
git clone git@gitlab.com:mbowcutt/ff4tv.git
```

# Branching

Branching is the good practice of forking the code repository when you work on a new feature. Branch the code when you start working on a task. This creates a bubble of isolation so that you might not step on other coders and cause issues. You'll make some commits for the branch and push them every now and then. When your branch is ready to go into production, make a merge request.

Currently, we have six branches for the purposes briefly described

* master
    - production branch, is directly tied to the [Heroku deploymenr](https://ff4tv.herokuapp.com/)
* imdb
    - integrate the `imdb-api` node module
    - search for shows
    - score shows
* design
    - create frontend material using `Bootstrap.js`
    - design the Home and Edit page layout
    - design the client interfaces
        - Roster Add/Drop
        - Roster Set Next
        - Roster Info
        - League Join
        - League Info
        - Edit User Settings
        - Edit League Settings
* backend-mvc
    - code object models
    - store and retrieve objects from database
    - provide object info to client
    - process client object change request
* backend-db
    - implement a PostgreSQL database
    - integrate th `pg` node module
* auth
    - integrate Google OAuth
    - redirect client to login if not logged in
    - code user object info

To make a new branch

```
git branch branchname
```

To switch to a branch

```
git checkout branchname
```

Always make sure you are on the correct branch before committing, pushing, and merging.

# Pulling and Rebasing

To safely get the most up to date code from a branch

```
git pull --rebase
```

# Committing

When you make a git commit, you're adding functional code to a branch (usually a development branch). Before you make a commit, you must stage the changes made. An easy way to do this is to give git the filename of the files to stage.

```
git add filename
```

Then, make sure you're on a good branch and commit the change with a helpful message.

```
git commit -m "These are my changes and this is what I did"
```

Now they are locked in! You may repeat this very often even for small changes. They add up :)

# Pushing

If you're developing locally and have some changes you want to publish, you need to push it to a shared branch on GitLab.

```
git push origin/branchname
```

When you push to a branch you'll be given a helpful link to make a merge request.

# Merging

When a feature is complete and ready to be implemented, we'll merge a development branch into master. This can be helpfully done on the GitLab repository website. To begin, go to branchname.

1. Click the blue "Create merge request" buttion, and verify that it is from branch `yourname` into `master`.
2. Write a description, and click confirm.
3. Go to the Merge Requests page, which is the button on the left bar with two mostly parralel lines.
4. View the Open requests, and click the green "merge" button. Do not delete the source afterward, as that kills the branch.
5. Note you can also Close merge requests from here, and view all other Merge Request history.

# Helpful tips

## Show Status

Using `git status` is a helpful command to show staged, added, changed, or deleted files.